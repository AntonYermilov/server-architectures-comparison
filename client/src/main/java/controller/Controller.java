package controller;

import client.ClientManager;
import javafx.application.Platform;
import javafx.beans.binding.BooleanBinding;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.nio.file.Paths;

import static client.ClientManager.TestResults;

public class Controller {

    public @FXML TextField hostField;
    public @FXML TextField portField;

    public @FXML ChoiceBox choiceBox;

    public @FXML TextField queriesField;

    public @FXML TextField elementsField;
    public @FXML TextField elementsFromField;
    public @FXML TextField elementsStepsField;
    public @FXML TextField elementsGapField;

    public @FXML TextField clientsField;
    public @FXML TextField clientsFromField;
    public @FXML TextField clientsStepsField;
    public @FXML TextField clientsGapField;

    public @FXML TextField deltaField;
    public @FXML TextField deltaFromField;
    public @FXML TextField deltaStepsField;
    public @FXML TextField deltaGapField;

    public @FXML ToggleGroup toggleGroup;
    public @FXML Toggle elementsToggle;
    public @FXML Toggle clientsToggle;
    public @FXML Toggle deltaToggle;

    public @FXML Button runButton;

    public @FXML LineChart<Number, Number> lineChart;
    public @FXML NumberAxis xAxis;
    public @FXML NumberAxis yAxis;

    private static BooleanBinding alwaysTrue = new BooleanBinding() {
        @Override
        protected boolean computeValue() {
            return true;
        }
    };
    private static BooleanBinding alwaysFalse = new BooleanBinding() {
        @Override
        protected boolean computeValue() {
            return false;
        }
    };

    private static boolean error = false;


    public @FXML void initialize() {
        elementsField.disableProperty().bind(alwaysTrue);
        elementsFromField.disableProperty().bind(alwaysTrue);
        elementsStepsField.disableProperty().bind(alwaysTrue);
        elementsGapField.disableProperty().bind(alwaysTrue);

        clientsField.disableProperty().bind(alwaysTrue);
        clientsFromField.disableProperty().bind(alwaysTrue);
        clientsStepsField.disableProperty().bind(alwaysTrue);
        clientsGapField.disableProperty().bind(alwaysTrue);

        deltaField.disableProperty().bind(alwaysTrue);
        deltaFromField.disableProperty().bind(alwaysTrue);
        deltaStepsField.disableProperty().bind(alwaysTrue);
        deltaGapField.disableProperty().bind(alwaysTrue);

        lineChart.setVisible(false);
        yAxis.setLabel("Time (ms)");
    }

    public @FXML void radioButtonListener() {
        elementsField.disableProperty().bind(elementsToggle.isSelected() ? alwaysTrue : alwaysFalse);
        elementsFromField.disableProperty().bind(elementsToggle.isSelected() ? alwaysFalse : alwaysTrue);
        elementsStepsField.disableProperty().bind(elementsToggle.isSelected() ? alwaysFalse : alwaysTrue);
        elementsGapField.disableProperty().bind(elementsToggle.isSelected() ? alwaysFalse : alwaysTrue);

        clientsField.disableProperty().bind(clientsToggle.isSelected() ? alwaysTrue : alwaysFalse);
        clientsFromField.disableProperty().bind(clientsToggle.isSelected() ? alwaysFalse : alwaysTrue);
        clientsStepsField.disableProperty().bind(clientsToggle.isSelected() ? alwaysFalse : alwaysTrue);
        clientsGapField.disableProperty().bind(clientsToggle.isSelected() ? alwaysFalse : alwaysTrue);

        deltaField.disableProperty().bind(deltaToggle.isSelected() ? alwaysTrue : alwaysFalse);
        deltaFromField.disableProperty().bind(deltaToggle.isSelected() ? alwaysFalse : alwaysTrue);
        deltaStepsField.disableProperty().bind(deltaToggle.isSelected() ? alwaysFalse : alwaysTrue);
        deltaGapField.disableProperty().bind(deltaToggle.isSelected() ? alwaysFalse : alwaysTrue);

        elementsField.clear();
        elementsFromField.clear();
        elementsStepsField.clear();
        elementsGapField.clear();

        clientsField.clear();
        clientsFromField.clear();
        clientsStepsField.clear();
        clientsGapField.clear();

        deltaField.clear();
        deltaFromField.clear();
        deltaStepsField.clear();
        deltaGapField.clear();

        clearToggleParameters();
    }

    private boolean checkParameters() {
        clearToggleParameters();

        boolean correct = true;

        if (hostField.getText().isEmpty()) {
            addProperty(hostField, "error");
            correct = false;
        }

        if (!isInteger(portField.getText())) {
            addProperty(portField, "error");
            correct = false;
        }

        if (!isInteger(queriesField.getText())) {
            addProperty(queriesField, "error");
            correct = false;
        }

        if (toggleGroup.getSelectedToggle() == null || !elementsField.isDisabled() && !isInteger(elementsField.getText())) {
            addProperty(elementsField, "error");
            correct = false;
        }

        if (toggleGroup.getSelectedToggle() == null || !elementsFromField.isDisabled() && !isInteger(elementsFromField.getText())) {
            addProperty(elementsFromField, "error");
            correct = false;
        }

        if (toggleGroup.getSelectedToggle() == null || !elementsStepsField.isDisabled() && !isInteger(elementsStepsField.getText())) {
            addProperty(elementsStepsField, "error");
            correct = false;
        }

        if (toggleGroup.getSelectedToggle() == null || !elementsGapField.isDisabled() && !isInteger(elementsGapField.getText())) {
            addProperty(elementsGapField, "error");
            correct = false;
        }

        if (toggleGroup.getSelectedToggle() == null || !clientsField.isDisabled() && !isInteger(clientsField.getText())) {
            addProperty(clientsField, "error");
            correct = false;
        }

        if (toggleGroup.getSelectedToggle() == null || !clientsFromField.isDisabled() && !isInteger(clientsFromField.getText())) {
            addProperty(clientsFromField, "error");
            correct = false;
        }

        if (toggleGroup.getSelectedToggle() == null || !clientsStepsField.isDisabled() && !isInteger(clientsStepsField.getText())) {
            addProperty(clientsStepsField, "error");
            correct = false;
        }

        if (toggleGroup.getSelectedToggle() == null || !clientsGapField.isDisabled() && !isInteger(clientsGapField.getText())) {
            addProperty(clientsGapField, "error");
            correct = false;
        }

        if (toggleGroup.getSelectedToggle() == null || !deltaField.isDisabled() && !isInteger(deltaField.getText())) {
            addProperty(deltaField, "error");
            correct = false;
        }

        if (toggleGroup.getSelectedToggle() == null || !deltaFromField.isDisabled() && !isInteger(deltaFromField.getText())) {
            addProperty(deltaFromField, "error");
            correct = false;
        }

        if (toggleGroup.getSelectedToggle() == null || !deltaStepsField.isDisabled() && !isInteger(deltaStepsField.getText())) {
            addProperty(deltaStepsField, "error");
            correct = false;
        }

        if (toggleGroup.getSelectedToggle() == null || !deltaGapField.isDisabled() && !isInteger(deltaGapField.getText())) {
            addProperty(deltaGapField, "error");
            correct = false;
        }

        return correct;
    }

    private void clearToggleParameters() {
        removeProperty(hostField, "error");
        removeProperty(portField, "error");
        removeProperty(queriesField, "error");
        removeProperty(elementsField, "error");
        removeProperty(elementsFromField, "error");
        removeProperty(elementsStepsField, "error");
        removeProperty(elementsGapField, "error");
        removeProperty(clientsField, "error");
        removeProperty(clientsFromField, "error");
        removeProperty(clientsStepsField, "error");
        removeProperty(clientsGapField, "error");
        removeProperty(deltaField, "error");
        removeProperty(deltaFromField, "error");
        removeProperty(deltaStepsField, "error");
        removeProperty(deltaGapField, "error");
    }

    public @FXML void startEvaluation() {
        if (!checkParameters()) {
            return;
        }

        ClientManager.initialize(hostField.getText(), Integer.parseInt(portField.getText()), (String) choiceBox.getValue());

        new Thread(() -> {
            Platform.runLater(() -> {
                runButton.setDisable(true);
                lineChart.setVisible(false);
            });
            error = false;

            if (toggleGroup.getSelectedToggle().equals(elementsToggle)) {
                Platform.runLater(() -> xAxis.setLabel("Elements"));
                iterateOverElements();
            }
            if (toggleGroup.getSelectedToggle().equals(clientsToggle)) {
                Platform.runLater(() -> xAxis.setLabel("Clients"));
                iterateOverClients();
            }
            if (toggleGroup.getSelectedToggle().equals(deltaToggle)) {
                Platform.runLater(() -> xAxis.setLabel("Delta"));
                iterateOverDelta();
            }

            Platform.runLater(() -> {
                runButton.setDisable(false);
                if (!error) {
                    lineChart.setVisible(true);
                }
            });
        }).start();
    }

    @SuppressWarnings("all")
    private void addProperty(TextField textField, String property) {
        if (!textField.getStyleClass().contains(property)) {
            textField.getStyleClass().add(property);
        }
    }

    @SuppressWarnings("all")
    private void removeProperty(TextField textField, String property) {
        if (textField.getStyleClass().contains(property)) {
            textField.getStyleClass().remove(property);
        }
    }

    @SuppressWarnings("all")
    private boolean isInteger(String s) {
        try {
            return Integer.parseInt(s) >= 0;
        } catch (NumberFormatException ignore) {
            return false;
        }
    }


    @SuppressWarnings("all")
    private void iterateOverElements() {
        int queries = Integer.parseInt(queriesField.getText());
        int clients = Integer.parseInt(clientsField.getText());
        int delta = Integer.parseInt(deltaField.getText());

        int elementsFrom = Integer.parseInt(elementsFromField.getText());
        int elementsSteps = Integer.parseInt(elementsStepsField.getText());
        int elementsGap = Integer.parseInt(elementsGapField.getText());

        ObservableList<XYChart.Series<Number, Number>> lines = FXCollections.observableArrayList(
                new LineChart.Series<Number, Number>("Total time", FXCollections.observableArrayList()),
                new LineChart.Series<Number, Number>("Server time", FXCollections.observableArrayList()),
                new LineChart.Series<Number, Number>("Sort time", FXCollections.observableArrayList())
        );

        writeParameters("elements", "total", getServerType(), queries, -1, clients, delta);
        writeParameters("elements", "server", getServerType(), queries, -1, clients, delta);
        writeParameters("elements", "sort", getServerType(), queries, -1, clients, delta);

        for (int cur = 0; cur < elementsSteps; cur++) {
            int elements = elementsFrom + cur * elementsGap;
            TestResults results = ClientManager.runClients(clients, queries, elements, delta);
            if (results != null) {
                writePoint("elements", "total", getServerType(), elements, results.averageTimeLocal);
                writePoint("elements", "server", getServerType(), elements, results.averageTimeServer);
                writePoint("elements", "sort", getServerType(), elements, results.averageTimeSort);

                lines.get(0).getData().add(new XYChart.Data<>(elements, results.averageTimeLocal));
                lines.get(1).getData().add(new XYChart.Data<>(elements, results.averageTimeServer));
                lines.get(2).getData().add(new XYChart.Data<>(elements, results.averageTimeSort));
            } else {
                error = true;
                return;
            }
        }

        Platform.runLater(() -> lineChart.setData(lines));
    }

    @SuppressWarnings("all")
    private void iterateOverClients() {
        int queries = Integer.parseInt(queriesField.getText());
        int elements = Integer.parseInt(elementsField.getText());
        int delta = Integer.parseInt(deltaField.getText());

        int clientsFrom = Integer.parseInt(clientsFromField.getText());
        int clientsSteps = Integer.parseInt(clientsStepsField.getText());
        int clientsGap = Integer.parseInt(clientsGapField.getText());

        ObservableList<XYChart.Series<Number, Number>> lines = FXCollections.observableArrayList(
                new LineChart.Series<Number, Number>("Total time", FXCollections.observableArrayList()),
                new LineChart.Series<Number, Number>("Server time", FXCollections.observableArrayList()),
                new LineChart.Series<Number, Number>("Sort time", FXCollections.observableArrayList())
        );

        writeParameters("clients", "total", getServerType(), queries, elements, -1, delta);
        writeParameters("clients", "server", getServerType(), queries, elements, -1, delta);
        writeParameters("clients", "sort", getServerType(), queries, elements, -1, delta);

        for (int cur = 0; cur < clientsSteps; cur++) {
            int clients = clientsFrom + cur * clientsGap;
            TestResults results = ClientManager.runClients(clients, queries, elements, delta);
            if (results != null) {
                writePoint("clients", "total", getServerType(), clients, results.averageTimeLocal);
                writePoint("clients", "server", getServerType(), clients, results.averageTimeServer);
                writePoint("clients", "sort", getServerType(), clients, results.averageTimeSort);

                lines.get(0).getData().add(new XYChart.Data<>(clients, results.averageTimeLocal));
                lines.get(1).getData().add(new XYChart.Data<>(clients, results.averageTimeServer));
                lines.get(2).getData().add(new XYChart.Data<>(clients, results.averageTimeSort));
            } else {
                error = true;
                return;
            }
        }

        Platform.runLater(() -> lineChart.setData(lines));
    }

    @SuppressWarnings("all")
    private void iterateOverDelta() {
        int queries = Integer.parseInt(queriesField.getText());
        int elements = Integer.parseInt(elementsField.getText());
        int clients = Integer.parseInt(clientsField.getText());

        int deltaFrom = Integer.parseInt(deltaFromField.getText());
        int deltaSteps = Integer.parseInt(deltaStepsField.getText());
        int deltaGap = Integer.parseInt(deltaGapField.getText());

        ObservableList<XYChart.Series<Number, Number>> lines = FXCollections.observableArrayList(
                new LineChart.Series<Number, Number>("Total time", FXCollections.observableArrayList()),
                new LineChart.Series<Number, Number>("Server time", FXCollections.observableArrayList()),
                new LineChart.Series<Number, Number>("Sort time", FXCollections.observableArrayList())
        );

        writeParameters("delta", "total", getServerType(), queries, elements, clients, -1);
        writeParameters("delta", "server", getServerType(), queries, elements, clients, -1);
        writeParameters("delta", "sort", getServerType(), queries, elements, clients, -1);

        for (int cur = 0; cur < deltaSteps; cur++) {
            int delta = deltaFrom + cur * deltaGap;
            TestResults results = ClientManager.runClients(clients, queries, elements, delta);
            if (results != null) {
                writePoint("delta", "total", getServerType(), delta, results.averageTimeLocal);
                writePoint("delta", "server", getServerType(), delta, results.averageTimeServer);
                writePoint("delta", "sort", getServerType(), delta, results.averageTimeSort);

                lines.get(0).getData().add(new XYChart.Data<>(delta, results.averageTimeLocal));
                lines.get(1).getData().add(new XYChart.Data<>(delta, results.averageTimeServer));
                lines.get(2).getData().add(new XYChart.Data<>(delta, results.averageTimeSort));
            } else {
                error = true;
                return;
            }
        }

        Platform.runLater(() -> lineChart.setData(lines));
    }

    private String getServerType() {
        String type = (String) choiceBox.getValue();
        if (type.equals("Standard server")) {
            return "standard";
        }
        if (type.equals("Blocking server")) {
            return "blocking";
        }
        if (type.equals("Non-blocking server")) {
            return "nonblocking";
        }
        return null;
    }

    private void writeParameters(String parameter, String metrics, String serverType, int ...option) {
        String filename = parameter + "-" + metrics + "-" + serverType;
        File file = Paths.get("results", filename).toFile();
        try (PrintWriter writer = new PrintWriter(new FileOutputStream(file, true))) {
            writer.println("ServerType: " + serverType);
            writer.println("xAxis: " + parameter);
            writer.println("yAxis: " + metrics);
            writer.println("Queries: " + option[0]);
            writer.println("Elements: " + option[1]);
            writer.println("Clients: " + option[2]);
            writer.println("Delta: " + option[3]);
        } catch (FileNotFoundException e) {
            System.err.println("File " + filename + " was not found");
        }
    }

    private void writePoint(String parameter, String metrics, String serverType, int x, long y) {
        String filename = parameter + "-" + metrics + "-" + serverType;
        File file = Paths.get("results", filename).toFile();
        try (PrintWriter writer = new PrintWriter(new FileOutputStream(file, true))) {
            writer.println(x + " " + y);
        } catch (FileNotFoundException e) {
            System.err.println("File " + filename + " was not found");
        }
    }

}
