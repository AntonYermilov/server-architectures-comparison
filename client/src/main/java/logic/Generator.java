package logic;


import java.util.ArrayList;
import java.util.Random;

public class Generator {
    private static final Random randomSeedGenerator = new Random(13);

    public static ArrayList<Integer> generate(int size) {
        Random random;
        synchronized (randomSeedGenerator) {
            random = new Random(randomSeedGenerator.nextInt());
        }

        ArrayList<Integer> array = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            array.add(random.nextInt());
        }
        return array;
    }
}
