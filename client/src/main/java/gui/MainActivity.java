package gui;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class MainActivity extends Application {

    private static int WINDOW_WIDTH = 550;
    private static int WINDOW_HEIGHT = 600;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        String mainLayout = "/MainLayout.fxml";

        try {
            Parent layout = FXMLLoader.load(MainActivity.class.getResource(mainLayout));
            Scene scene = new Scene(layout, WINDOW_WIDTH, WINDOW_HEIGHT);

            primaryStage.setScene(scene);
            primaryStage.setResizable(false);
            primaryStage.show();
        } catch (IOException e) {
            System.err.println("An error occurred while loading " + mainLayout);
        }
    }
}
