package client;

import logic.Generator;
import proto.Array;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.SocketChannel;
import java.util.*;

import static proto.Array.Data;
import static proto.Options.Architecture;

public class ClientManager {

    private static String hostName;
    private static int portNumber;
    private static String architectureType;

    private static long totalTimeLocal;
    private static long totalTimeServer;
    private static long totalTimeSort;

    public static void initialize(String hostName, int portNumber, String type) {
        ClientManager.hostName = hostName;
        ClientManager.portNumber = portNumber;

        if (type.equals("Standard server")) {
            ClientManager.architectureType = "STANDARD";
        }
        if (type.equals("Blocking server")) {
            ClientManager.architectureType = "BLOCKING";
        }
        if (type.equals("Non-blocking server")) {
            ClientManager.architectureType = "NONBLOCKING";
        }
    }

    public static TestResults runClients(int clients, int queries, int elements, int delta) {
        Architecture.Meta meta = Architecture.Meta.newBuilder()
                .setClients(clients)
                .setQueries(queries)
                .build();

        Architecture architecture = Architecture.newBuilder()
                .setType(Architecture.Type.valueOf(architectureType))
                .setMeta(meta)
                .build();

        try (SocketChannel socketChannel = SocketChannel.open(new InetSocketAddress(hostName, portNumber))) {
            writeData(socketChannel, ByteBuffer.wrap(architecture.toByteArray()));
        } catch (IOException e) {
            System.err.println("An error occurred while interacting with server (client manager):");
            System.err.print(e.getMessage());
            return null;
        }

        sleep(1000);

        totalTimeLocal = 0;
        totalTimeServer = 0;
        totalTimeSort = 0;

        Thread[] clientThreads = new Thread[clients];
        for (int i = 0; i < clients; i++) {
            clientThreads[i] = new Thread(() -> new Client().interact(queries, elements, delta));
            clientThreads[i].setDaemon(true);
            clientThreads[i].start();
        }

        for (Thread thread : clientThreads) {
            while (thread.isAlive()) {
                try {
                    thread.join();
                } catch (InterruptedException ignored) {
                }
            }
        }

        return new TestResults(totalTimeLocal / clients, totalTimeServer / clients, totalTimeSort / clients);
    }

    private static class Client {
        public void interact(int queries, int elements, int delta) {
            try (SocketChannel socketChannel = SocketChannel.open(new InetSocketAddress(hostName, portNumber))) {
                long timeLocal = System.currentTimeMillis();
                long timeServer = 0;
                long timeSort = 0;

                for (int query = 0; query < queries; query++) {
                    ArrayList<Integer> array = Generator.generate(elements);

                    Data.Builder builder = Data.newBuilder();
                    builder.setSize(array.size());
                    builder.addAllArray(array);

                    writeData(socketChannel, ByteBuffer.wrap(builder.build().toByteArray()));

                    Data data = Data.parseFrom(readData(socketChannel));
                    timeServer += data.getMeta().getProcessTime();
                    timeSort += data.getMeta().getSortTime();

                    if (query + 1 < queries) {
                        sleep(delta);
                    }
                }

                timeLocal = System.currentTimeMillis() - timeLocal;
                update(timeLocal / queries, timeServer / queries, timeSort / queries);

            } catch (IOException e) {
                System.err.println("An error occurred while interacting with server (client)");
                System.err.print(e.getMessage());
            }
        }

        private void update(long timeLocal, long timeServer, long timeSort) {
            synchronized (ClientManager.class) {
                totalTimeLocal += timeLocal;
                totalTimeServer += timeServer;
                totalTimeSort += timeSort;
            }
        }
    }

    public static class TestResults {
        public final long averageTimeLocal;
        public final long averageTimeServer;
        public final long averageTimeSort;

        public TestResults(long averageTimeLocal, long averageTimeServer, long averageTimeSort) {
            this.averageTimeLocal = averageTimeLocal;
            this.averageTimeServer = averageTimeServer;
            this.averageTimeSort = averageTimeSort;
        }
    }

    private static int readCapacity(SocketChannel socketChannel) throws IOException {
        ByteBuffer capacityBuffer = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN);
        socketChannel.read(capacityBuffer);
        return ((ByteBuffer) capacityBuffer.flip()).getInt();
    }

    private static ByteBuffer readData(SocketChannel socketChannel) throws IOException {
        int capacity = readCapacity(socketChannel);
        ByteBuffer dataBuffer = ByteBuffer.allocate(capacity);
        while (capacity > 0) {
            capacity -= socketChannel.read(dataBuffer);
        }
        return (ByteBuffer) dataBuffer.flip();
    }

    private static void writeData(SocketChannel socketChannel, ByteBuffer dataBuffer) throws IOException {
        socketChannel.write((ByteBuffer) ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(dataBuffer.capacity()).flip());
        while (dataBuffer.hasRemaining()) {
            socketChannel.write(dataBuffer);
        }
    }

    private static void sleep(int millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException ignore) {}
    }
}
