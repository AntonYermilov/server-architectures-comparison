package logic;

import proto.Array.Data;

import java.util.ArrayList;

public class Logic {

    public static Data.Builder processData(Data data) {
        long timeSort = System.currentTimeMillis();

        ArrayList<Integer> array = new ArrayList<>(data.getArrayList());
        sort(array);
        Data.Builder serverDataBuilder = Data.newBuilder();
        serverDataBuilder.setSize(array.size());
        serverDataBuilder.addAllArray(array);

        timeSort = System.currentTimeMillis() - timeSort;

        return serverDataBuilder.setMeta(Data.Meta.newBuilder().setSortTime(timeSort).setProcessTime(0));
    }

    private static void sort(ArrayList<Integer> array) {
        for (int i = 0; i < array.size(); i++) {
            for (int j = i + 1; j < array.size(); j++) {
                if (array.get(j) < array.get(i)) {
                    int tmp = array.get(i);
                    array.set(i, array.get(j));
                    array.set(j, tmp);
                }
            }
        }
    }

}
