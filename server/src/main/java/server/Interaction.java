package server;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.SocketChannel;

public class Interaction {

    public static int readCapacity(SocketChannel socketChannel) throws IOException {
        ByteBuffer capacityBuffer = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN);
        socketChannel.read(capacityBuffer);
        return ((ByteBuffer) capacityBuffer.flip()).getInt();
    }

    public static void writeCapacity(SocketChannel socketChannel, int capacity) throws IOException {
        ByteBuffer capacityBuffer = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(capacity);
        socketChannel.write((ByteBuffer) capacityBuffer.flip());
    }

    public static ByteBuffer readData(SocketChannel socketChannel) throws IOException {
        int capacity = readCapacity(socketChannel);
        ByteBuffer dataBuffer = ByteBuffer.allocate(capacity);
        while (capacity > 0) {
            capacity -= socketChannel.read(dataBuffer);
        }
        return (ByteBuffer) dataBuffer.flip();
    }

    public static void writeData(SocketChannel socketChannel, ByteBuffer dataBuffer) throws IOException {
        writeCapacity(socketChannel, dataBuffer.capacity());
        while (dataBuffer.hasRemaining()) {
            socketChannel.write(dataBuffer);
        }
    }

    public static void readDataNonBlocking(SocketChannel socketChannel, ByteBuffer dataBuffer) throws IOException {
        while (dataBuffer.hasRemaining()) {
            if (socketChannel.read(dataBuffer) == 0) {
                break;
            }
        }
    }

    public static void writeDataNonBlocking(SocketChannel socketChannel, ByteBuffer dataBuffer) throws IOException {
        while (dataBuffer.hasRemaining()) {
            if (socketChannel.write(dataBuffer) == 0) {
                break;
            }
        }
    }

}
