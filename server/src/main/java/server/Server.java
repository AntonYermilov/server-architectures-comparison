package server;

import proto.Options.Architecture;
import server.architecture.Blocking;
import server.architecture.Nonblocking;
import server.architecture.ServerArchitecture;
import server.architecture.Standard;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;

import static server.Interaction.readData;

public class Server {

    private static Architecture architectureInfo;
    private static ServerArchitecture architecture;
    private static ArrayList<Thread> threads;

    public static void main(String[] args) {
        if (args.length != 1) {
            System.out.println("Expected exactly one argument: number of port");
            return;
        }

        try {
            runServer(Integer.parseInt(args[0]));
        } catch (NumberFormatException e) {
            System.out.println("Number of port should be an integer");
        }
    }

    private static void runServer(int port) {
        try (ServerSocketChannel serverSocketChannel = ServerSocketChannel.open()) {
            serverSocketChannel.bind(new InetSocketAddress(port));
            System.out.println("Server is running on port " + port);

            while (true) {
                final SocketChannel clientManagerSocketChannel = serverSocketChannel.accept();

                architectureInfo = Architecture.parseFrom(readData(clientManagerSocketChannel));
                int clients = architectureInfo.getMeta().getClients();
                int queries = architectureInfo.getMeta().getQueries();

                threads = new ArrayList<>();

                switch (architectureInfo.getType()) {
                    case STANDARD:
                        System.out.println("Standard architecture was chosen");
                        architecture = new Standard();
                        break;
                    case BLOCKING:
                        System.out.println("Blocking architecture was chosen");
                        architecture = new Blocking();
                        break;
                    case NONBLOCKING:
                        System.out.println("Non-blocking architecture was chosen");
                        architecture = new Nonblocking(clients);
                        break;
                }

                clientManagerSocketChannel.close();

                for (int client = 0; client < clients; client++) {
                    final SocketChannel clientSocketChannel = serverSocketChannel.accept();
                    architecture.processConnection(clientSocketChannel, queries);
                }

                for (Thread thread : threads) {
                    while (thread.isAlive()) {
                        try {
                            thread.join();
                        } catch (InterruptedException ignore) {
                        }
                    }
                }
                architecture.shutdown();
            }

        } catch (IOException e) {
            System.err.println("An unexpected error occurred");
            System.err.println(e.getMessage());
        }
    }

    public static void addThread(Thread thread) {
        threads.add(thread);
    }

}
