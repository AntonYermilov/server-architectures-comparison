package server.architecture;

import logic.Logic;
import proto.Array.Data;
import server.Server;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

import static server.Interaction.readData;
import static server.Interaction.writeData;

public class Standard implements ServerArchitecture {

    public Standard() {}

    @Override
    public void processConnection(SocketChannel socketChannel, int queries) {
        Thread clientThread = new Thread(() -> {
            try {
                for (int query = 0; query < queries; query++) {
                    Data clientData = Data.parseFrom(readData(socketChannel));

                    long timeServer = System.currentTimeMillis();

                    Data.Builder serverDataBuilder = Logic.processData(clientData);

                    timeServer = System.currentTimeMillis() - timeServer;
                    serverDataBuilder.getMetaBuilder().setProcessTime(timeServer);

                    writeData(socketChannel, ByteBuffer.wrap(serverDataBuilder.build().toByteArray()));
                }
                socketChannel.close();
            } catch (IOException e) {
                System.err.println("An unexpected error occurred while interacting with client");
                System.err.println(e.getMessage());
            }
        });
        clientThread.start();
        Server.addThread(clientThread);
    }

    @Override
    public void shutdown() {}

}
