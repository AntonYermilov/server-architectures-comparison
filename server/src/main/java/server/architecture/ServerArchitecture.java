package server.architecture;

import java.nio.channels.SocketChannel;

public interface ServerArchitecture {
    void processConnection(SocketChannel socketChannel, int queries);
    void shutdown();
}
