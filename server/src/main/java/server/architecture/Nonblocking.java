package server.architecture;

import com.google.protobuf.InvalidProtocolBufferException;
import logic.Logic;
import proto.Array.Data;
import server.Interaction;
import server.Server;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Logger;

public class Nonblocking implements ServerArchitecture {

    private int clients;
    private final Object clientSync = new Object();

    private ExecutorService executorService;

    private Selector readSelector;
    private Selector writeSelector;

    private ConcurrentLinkedQueue<Client> readQueue = new ConcurrentLinkedQueue<>();
    private ConcurrentLinkedQueue<Client> writeQueue = new ConcurrentLinkedQueue<>();

    public Nonblocking(int clients) {
        this.clients = clients;
        executorService = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
        try {
            readSelector = Selector.open();
            Thread readThread = new Thread(this::processRead);

            writeSelector = Selector.open();
            Thread writeThread = new Thread(this::processWrite);

            readThread.start();
            writeThread.start();

            Server.addThread(readThread);
            Server.addThread(writeThread);
        } catch (IOException e) {
            System.err.println("An error occurred while opening selector");
            System.err.println(e.getMessage());
        }
    }

    @Override
    public void processConnection(SocketChannel socketChannel, int queries) {
        try {
            socketChannel.configureBlocking(false);
            new Client(socketChannel, queries).registerRead();
        } catch (IOException e) {
            System.err.println("An error occurred while making channel non-blocking");
            System.err.println(e.getMessage());
        }
    }

    @Override
    public void shutdown() {
        executorService.shutdownNow();
    }

    private class Client {
        SocketChannel socketChannel;
        int queries;

        long timeServer;

        ByteBuffer inputBuffer;
        ByteBuffer outputBuffer;

        Data.Builder dataBuilder;

        Client(SocketChannel socketChannel, int queries) {
            this.socketChannel = socketChannel;
            this.queries = queries;
        }

        void registerRead() {
            readQueue.add(this);
            readSelector.wakeup();
        }

        void registerWrite() {
            writeQueue.add(this);
            writeSelector.wakeup();
        }

        synchronized void startTimer() {
            timeServer = System.currentTimeMillis();
        }

        synchronized void stopTimer() {
            timeServer = System.currentTimeMillis() - timeServer;
            dataBuilder.getMetaBuilder().setProcessTime(timeServer);
        }

        void readRequest() throws IOException {
            if (inputBuffer == null) {
                inputBuffer = ByteBuffer.allocate(Interaction.readCapacity(socketChannel));
            }
            Interaction.readDataNonBlocking(socketChannel, inputBuffer);
        }

        boolean requestReceived() {
            return !inputBuffer.hasRemaining();
        }

        void writeResponse() throws IOException {
            if (outputBuffer == null) {
                stopTimer();
                synchronized (this) {
                    outputBuffer = ByteBuffer.wrap(dataBuilder.build().toByteArray());
                }
                Interaction.writeCapacity(socketChannel, outputBuffer.capacity());
            }
            Interaction.writeDataNonBlocking(socketChannel, outputBuffer);
        }

        boolean responseSent() {
            return !outputBuffer.hasRemaining();
        }

        void startProcessing() {
            inputBuffer.flip();
            startTimer();
            executorService.submit(() -> {
                try {
                    Data clientData = Data.parseFrom(inputBuffer);

                    synchronized (Client.this) {
                        dataBuilder = Logic.processData(clientData);
                    }

                    registerWrite();
                } catch (InvalidProtocolBufferException e) {
                    System.err.println("Received data is incorrect");
                    System.err.println(e.getMessage());
                }
            });
        }

        synchronized void finishProcessing() {
            queries--;
            if (queries == 0) {
                try {
                    socketChannel.close();
                } catch (IOException e) {
                    System.err.println("Impossible to close socket channel");
                    System.err.println(e.getMessage());
                }
                synchronized (clientSync) {
                    clients--;
                }
            }
            if (clients == 0) {
                readSelector.wakeup();
                writeSelector.wakeup();
            }

            inputBuffer = null;
            outputBuffer = null;
            dataBuilder = null;
        }
    }

    private void processRead() {
        try {
            while (clients > 0) {
                while (!readQueue.isEmpty()) {
                    Client client = readQueue.poll();
                    client.socketChannel.register(readSelector, SelectionKey.OP_READ, client);
                }

                int found = readSelector.select();
                if (found == 0) {
                    continue;
                }

                Set<SelectionKey> selectionKeys = readSelector.selectedKeys();
                Iterator<SelectionKey> iterator = selectionKeys.iterator();

                while (iterator.hasNext()) {
                    SelectionKey key = iterator.next();
                    Client client = (Client) key.attachment();

                    client.readRequest();
                    if (client.requestReceived()) {
                        client.startProcessing();
                    }

                    iterator.remove();
                }

            }
        } catch (IOException e) {
            System.err.println("An unexpected IOError occurred while searching for ready channels");
            System.err.println(e.getMessage());
        }
    }

    private void processWrite() {
        try {
            while (clients > 0) {
                while (!writeQueue.isEmpty()) {
                    Client client = writeQueue.poll();
                    client.socketChannel.register(writeSelector, SelectionKey.OP_WRITE, client);
                }

                int found = writeSelector.select();
                if (found == 0) {
                    continue;
                }

                Set<SelectionKey> selectionKeys = writeSelector.selectedKeys();
                Iterator<SelectionKey> iterator = selectionKeys.iterator();

                while (iterator.hasNext()) {
                    SelectionKey key = iterator.next();
                    Client client = (Client) key.attachment();

                    client.writeResponse();
                    if (client.responseSent()) {
                        key.cancel();
                        client.finishProcessing();
                    }

                    iterator.remove();
                }

            }
        } catch (IOException e) {
            System.err.println("An unexpected IOError occurred while searching for ready channels");
            System.err.println(e.getMessage());
        }
    }
}
