package server.architecture;

import logic.Logic;
import proto.Array;
import server.Server;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import static server.Interaction.readData;
import static server.Interaction.writeData;

public class Blocking implements ServerArchitecture {

    private ExecutorService executorService;

    public Blocking() {
        executorService = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
    }

    @Override
    public void processConnection(SocketChannel socketChannel, int queries) {
        final ExecutorService responseService = Executors.newSingleThreadExecutor();

        Thread requestThread = new Thread(new Runnable() {
            final List<Future> taskFutures = new ArrayList<>();
            final List<Future> responseFutures = new ArrayList<>();

            @Override
            public void run() {
                try {
                    for (int query = 0; query < queries; query++) {
                        Array.Data clientData = Array.Data.parseFrom(readData(socketChannel));

                        long timeServerStart = System.currentTimeMillis();

                        Future taskFuture = executorService.submit(() -> {
                            Array.Data.Builder serverDataBuilder = Logic.processData(clientData);
                            Future responseFuture = responseService.submit(() -> {
                                try {
                                    long timeServerFinish = System.currentTimeMillis();
                                    serverDataBuilder.getMetaBuilder().setProcessTime(timeServerFinish - timeServerStart);
                                    writeData(socketChannel, ByteBuffer.wrap(serverDataBuilder.build().toByteArray()));
                                } catch (IOException e) {
                                    System.err.println("An unexpected error occurred while sending data to client");
                                    System.err.println(e.getMessage());
                                }
                            });
                            synchronized (responseFutures) {
                                responseFutures.add(responseFuture);
                            }
                        });
                        taskFutures.add(taskFuture);
                    }

                    for (Future future : taskFutures) {
                        while (!future.isDone()) {
                            try {
                                future.get();
                            } catch (InterruptedException | ExecutionException ignore) {}
                        }
                    }
                    for (Future future : responseFutures) {
                        while (!future.isDone()) {
                            try {
                                future.get();
                            } catch (InterruptedException | ExecutionException ignore) {}
                        }
                    }

                    responseService.shutdownNow();
                    socketChannel.close();
                } catch (IOException e) {
                    System.err.println("An unexpected error occurred while receiving data from client");
                    System.err.println(e.getMessage());
                }
            }
        });
        requestThread.start();
        Server.addThread(requestThread);
    }

    @Override
    public void shutdown() {
        executorService.shutdownNow();
    }


}
