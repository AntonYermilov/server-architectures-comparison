import os
import matplotlib.pyplot as plt

OPTION = 'elements clients delta'.split()
METRICS = 'total server sort'.split()
ARCHITECTURE = 'standard blocking nonblocking'.split()
COLOR='red green blue'.split()

for xLabel in OPTION:
    for yLabel in METRICS:
        plt.xlabel(xLabel)
        plt.ylabel(yLabel + 'time (ms)')
        for tp, color in zip(ARCHITECTURE, COLOR):
            filename = os.path.join('results', '-'.join([xLabel, yLabel, tp]))
            with open(filename) as f:
                lines = f.readlines()[7:17]
                coord = list(map(lambda line : list(map(int, line.split())), lines))
                xs = list(map(lambda coord : coord[0], coord))
                ys = list(map(lambda coord : coord[1], coord))
                plt.plot(xs, ys, color=color, label=tp)
                plt.legend(bbox_to_anchor=(0., 1.02, 1., 1.02), loc=3, ncol=3, mode="expand", borderaxespad=0.)

        filename = os.path.join('results', '-'.join([xLabel, yLabel]) + '.png')
        plt.savefig(filename)
        plt.close()
